export const LoggedInRequest = "SIGN_IN_REQUEST";
export const LoggedInSuccess = "SIGN_IN_SUCCESS";
export const LoggedInFailure = "SIGN_IN_FAILURE";
export const LoggedInViaToken = "SIGN_IN_VIA_TOKEN";
export const LoggedOut = "SIGN_OUT";