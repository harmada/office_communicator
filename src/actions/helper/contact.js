export const SaveContactRequest = "SAVE_CONTACT_REQUEST";
export const SaveContactSuccess = "SAVE_CONTACT_SUCCESS";
export const SaveContactFailed = "SAVE_CONTACT_FAILED";
export const ResetContact = "SAVE_CONTACT_RESET";