export const SaveUsersRequest = "SAVE_USER_REQUEST";
export const SaveUserSuccess = "SAVE_USER_SUCCESS";
export const SaveUserFailed = "SAVE_USER_FAILURE";
export const DeleteUsers = "DELETE_USER";