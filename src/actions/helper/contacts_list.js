export const SaveContactListRequest = "SAVE_CONTACT_LIST_REQUEST";
export const SaveContactListSuccess = "SAVE_CONTACT_LIST_SUCCESS";
export const SaveContactListFailed = "SAVE_CONTACT_LIST_FAILED"; 
export const ResetContactList = "SAVE_CONTACT_LIST_RESET";