export const SaveChatRequest = "SAVE_CHAT_REQUEST";
export const SaveChatSuccess = "SAVE_CHAT_SUCCESS";
export const SaveChatFailed = "SAVE_CHAT_FAILED";
export const ResetChat = "SAVE_CHAT_RESET";