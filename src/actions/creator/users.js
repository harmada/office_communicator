import * as users from '../helper/users';

export const saveUser = (user) => {
  return { 
    type: users.SaveUserSuccess,
    payload: user
  }
}

export const deleteUser = () => {
  return {
    type: users.DeleteUsers,
  }
}