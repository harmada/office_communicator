import * as chat from "../helper/chat";
import { server } from "../../api/serverUrl";
import axios from "axios";

const CancelToken = axios.CancelToken;
const source = CancelToken.source()

export const fetchChatMessage = (payload) => async (dispatch) => {
  dispatch({ type: chat.SaveChatRequest })

  try {
    const response = await axios.get(`${server}/chatmessage?pageNum=${payload.pageNum}&chatRoomId=${payload.chatRoomId}`, { cancelToken: source.token })
    dispatch({ type: chat.SaveChatSuccess, payload: response.data })
  }
  catch (err) {
    dispatch({ type: chat.SaveChatFailed, error: err })
  }

}


export const fetchMoreMessage = (payload) =>  async (dispatch, getState) => {
  dispatch({ type: chat.SaveChatRequest })

  try {
    const response = await axios.get(`${server}/chatmessage?pageNum=${payload.pageNum}&chatRoomId=${payload.chatRoomId}`, { cancelToken: source.token })
    const prevChat = getState().chat.chat;    
    const newChat = {
      chat: [...new Set([
        ...prevChat.chat,
        ...response.data.chat
      ])]}
      
    dispatch({ type: chat.SaveChatSuccess, payload: newChat })
  }
  catch (err) {
    dispatch({ type: chat.SaveChatFailed, error: err })
  }
}

export const sendMessage = payload => async (dispatch) => {
  
}