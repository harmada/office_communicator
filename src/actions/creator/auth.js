import * as auth from "../helper/auth";
import * as user from "../helper/users";
import * as contact from "../helper/contact";
import * as contact_list from "../helper/contacts_list";
import { server } from "../../api/serverUrl";
import axios from "axios";

export const loggedIn = (payload) => async (dispatch) => {
  dispatch({ type: auth.LoggedInRequest });

  try {
    const response = await axios.post(`${server}/login`, payload);
    dispatch({ type: user.SaveUserSuccess, payload: response.data.user });
    setTimeout(() => {
      dispatch({ type: auth.LoggedInSuccess })
    }, 1000)
    localStorage.setItem('token', response.data.token)
  }
  catch ({ response }) {
    dispatch({ type: auth.LoggedInFailure, error: response.data });
  }
}
  

export const loggedInViaToken = (token) => async (dispatch) => {
  dispatch({ type: auth.LoggedInRequest });

  try { 
    const response = await axios.post(`${server}/me`, { token }, { headers: { "Authorization": `${token}` } })
    dispatch({ type: user.SaveUserSuccess, payload: response.data.userProfile });
    dispatch({ type: auth.LoggedInSuccess })
  }
  catch ({ response}) {
    dispatch({ type: auth.LoggedInFailure, error: response.data });
  }
}


export const loggedOut = () => dispatch => {
  dispatch({ type: user.DeleteUsers })
  dispatch({ type: contact.ResetContact })
  dispatch({ type: contact_list.ResetContactList })
  localStorage.removeItem('token');
}