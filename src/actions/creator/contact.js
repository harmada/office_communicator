import * as contacts from '../helper/contact';
import { server } from '../../api/serverUrl';
import axios from 'axios';


export const saveContact = (contact) =>  async (dispatch) => {

  dispatch({ type: contacts.SaveContactRequest })

  try {
    const payload = {
      chatRoomName: "Private Chat",
      type: "Personal",
      userId: contact.userId,
      receiverId: contact.follower._id,
    }

    const response = await axios.post(`${server}/chatroom`, payload)

    const contactDetails = {
      _id: contact.follower._id,
      image: contact.follower.image,
      firstName: contact.follower.firstName,
      lastName: contact.follower.lastName,
      jobTitle: contact.follower.jobTitle,
      company: contact.follower.company,
      chatRoomId: response.data.chatRoom._id,
    }

    dispatch({type: contacts.SaveContactSuccess, payload: contactDetails })
  }

  catch (err) {
    dispatch({ type: contacts.SaveContactFailed })
  }
}