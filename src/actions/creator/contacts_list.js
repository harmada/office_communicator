import * as contacts from '../helper/contacts_list';
import { server } from '../../api/serverUrl';
import axios from 'axios';

export const contactsList = (id) => async (dispatch) => {
  dispatch({ type: contacts.SaveContactListRequest})

  try {
    const payload = {
      _id: id
    }
    const response = await axios.post(`${server}/followers`, payload)
    dispatch({ type: contacts.SaveContactListSuccess, payload: response.data.followers })
  }
  catch ({ response }) {
    dispatch({ type: contacts.SaveContactListFailed})
  }


}