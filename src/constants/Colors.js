const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';
const darker = '#c19fe6';
const black = "#0e1111"
const secondary = "#b07ce8"
const mainpallet = "#F652A0"
const main = "#4C5270"
const tertiary = "#36EEE0"
const lines = "#BCECE0"

export default {
  light: {
  text: '#000',
  background: '#fff',
  tint: tintColorLight,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColorLight,
  },
  main: {
  text: '#000',
  background: '#fff',
  danger: '#DC143C',
  tint: main,
  tabIconDefault: '#ccc',
  tabIconSelected: darker,
  main: main,
  secondary: secondary,
  tertiary: tertiary,
  lines: lines,
  dirtyWhite: '#e6e6e6'
  },
  dark: {
  text: '#fff',
  background: '#000',
  tint: tintColorDark,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColorDark,
  },
  status: {
  online: '#00ff00',
  offline: '#696969',
  buzy: '#DC143C'
  },
  chat : {
  myChat: "#affaaf",
  otherChat: "#ecefe9"
  }
};
