
import * as users from '../actions/helper/users';

const initState = {
  user: {},
  loading: false,
  error: null
}

const userReducer = (state = initState, action ) => {
  switch (action.type) {
    case users.SaveUsersRequest:
      return { 
        ...state,
        loading: true,
        error: null
      }
    case users.SaveUserSuccess:
      return { 
        ...state,
        user: action.payload,
        loading: false
      }
    case users.SaveUserFailed:
      return { 
        ...state,
        error: action.error,
        loading: false
      }
    case users.DeleteUsers:
      return {
        ...state,
        ...initState,
      }
    default:
        return state;
  }
}

export default userReducer;