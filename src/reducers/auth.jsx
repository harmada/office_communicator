import * as auth from '../actions/helper/auth';

const initState = {
  auth: false,
  loading: false,
  error: null
}

const loggedReducer = (state = initState, action ) => {
  switch(action.type) {
    case auth.LoggedInRequest:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case auth.LoggedInSuccess:
      return {
        ...state,
        auth: true,
        loading: false
      };
    case auth.LoggedInFailure:
      return {
        ...state,
        auth: false,
        loading: false,
        error: action.error
      };
    case auth.LoggedOut:
      return {
        ...state,
        ...initState
      };
    default:
      return state;
  }
}

export default loggedReducer;