import * as contacts from '../actions/helper/contact';

const initState = { 
  contact: {},
  loading: false,
  error: null
}

const contactsReducer = ( state = initState, action ) => {
  switch (action.type) {
    case contacts.SaveContactRequest:
      return { 
        ...state, 
        loading: true, 
      };
    case contacts.SaveContactSuccess:
      return { 
        ...state, 
        loading: false,
        contact: action.payload 
      };
    case contacts.SaveContactFailed:
      return { 
        ...state, 
        loading: false,
        error: action.error
      };
    case contacts.ResetContact:
      return {
        ...state, 
        ...initState
      }
    default:
      return state;
  }
}

export default contactsReducer;