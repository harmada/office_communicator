import loggedReducer from "./auth"; 
import userReducer from "./user";
import contactsListReducer from "./contacts_list";
import contactsReducer  from "./contact";
import chatReducer  from "./chat";
import { combineReducers } from "redux"; 

const allReducer = combineReducers({
  auth: loggedReducer,
  user: userReducer,
  contactList: contactsListReducer,
  contact: contactsReducer,
  chat: chatReducer,
})

export default allReducer;