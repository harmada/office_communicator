import * as contacts from '../actions/helper/contacts_list';

const initState = {
  followers: [],
  loading: false,
  error: null
}

const contactsListReducer = ( state = initState , action ) => {
  switch (action.type) {
    case contacts.SaveContactListRequest:
      return {
        ...state,
        loading: true,
      }
    case contacts.SaveContactListSuccess:
      return {
        ...state,
        loading: false,
        followers: action.payload
      }
    case contacts.SaveContactListFailed:
      return {
        ...state,
        loading: false,
        error: action.error
      }
    case contacts.ResetContactList:
      return {
        ...state,
        ...initState
      }
    default:
      return state;
  }
}


export default contactsListReducer;