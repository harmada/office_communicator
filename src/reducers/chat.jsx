import * as chat from "../actions/helper/chat";

const initState = { 
  chat: [],
  loading: false,
  error: null
}


const chatReducer = (state = initState, action ) => {
  switch (action.type) {
    case chat.SaveChatRequest: 
      return {
        ...state,
        loading: true,
        error: null
      }
    case chat.SaveChatSuccess: 
      return {
        ...state,
        loading: false,
        chat: action.payload
      }
    case chat.SaveChatFailed: 
      return {
        ...state,
        loading : false,
        error: action.error
      }
    case chat.ResetChat: 
      return {
        ...state,
        ...initState
      }
    default: 
      return state

  }
}


export default chatReducer;