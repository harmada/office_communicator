import { server } from "../api/serverUrl"
import axios from "axios";

export const createChatRoom = async (data) => {
  const result = await axios.post(`${server}/chatroom`, data);
  return result;
}

export const getChatMessages = async (chatRoom) => {
	const result = await axios.get(`${server}/chatmessage?pageNum=${chatRoom.pageNum}&chatRoomId=${chatRoom.chatRoomId}`)
	return result;
}

export const generateChatRoomId = async ({ userId, receiverId }) => {
  const payload = {
    chatRoomName: "Private Chat",
    type: "Personal",
    userId,
    receiverId,
  }

  if(userId === undefined || receiverId === undefined) return null;

  const chat = await createChatRoom(payload)
  const { _id } = chat.data.chatRoom;
  return _id;
}


export const sendMessage = async (message) => {
  const result = await axios.post(`${server}/chat`, message )
	return result;
}