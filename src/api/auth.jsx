import { server } from './serverUrl';
import axios from 'axios';


export const updatePassword =  async (payload) => {
  try {
    const result = await axios.put(`${server}/password`, payload)
    return result;
  }
  catch ({ response }) {
    return response;
  }
}

export const loginAuth = async (payload) => {
  try {
    const result = await axios.post(`${server}/login`, payload)
    const { data: { token, user } } = result;
    localStorage.setItem('token', token)
    localStorage.setItem('userID', user._id)
    return result;
  }
  catch ({ response }) {
    const { data } = response;
    return data
  }
}

export const changePassword = async (payload) => {
  try {
    const result = await axios.post(`${server}/forgotpassword`, payload)
    return result;
  }
  catch ({ response }) {
    return response
  }
}

export const getDataFromToken = async () => {
  const token = localStorage.getItem('token');
  try {
    const user = await axios.post(`${server}/me`, { token }, { headers: { "Authorization": `${token}` } })
    const { data } = user;
    const { userProfile } = data;
    
    if(typeof userProfile === 'object') {
      return userProfile;
    }
  }
  catch ({ response }) {
    console.log('response', response)
  }
} 

export const logOut = async () => {
  localStorage.clear()
}