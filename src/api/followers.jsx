import { server } from './serverUrl';
import axios from 'axios';

export const follow = async (data) => {
  const result = await axios.post(`${server}/follow`, data);
  return result;
}

export const followers = async (data) => {
  const payload = { 
    _id: data
  }
  const result = await axios.post(`${server}/followers`, payload);
  return result;

}