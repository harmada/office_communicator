import React, { useEffect, useState } from 'react';
import { Link, useLocation, useHistory  } from 'react-router-dom';
import { updatePassword } from '../api/auth'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const ForgotPass = () => {
  let query = useQuery();

  const [email,] = useState(query.get('email'));
  const [token,] = useState(query.get('token'));
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');


  useEffect(() => {
  },[])

  const notifyError = (error) => toast.error(error);
  const notifyUser = (data) => toast.success(data);
  const history = useHistory();

  const changePassword = async () => {
    if( password !== confirmPassword || password.length === 0 ) {
      const error = "Password is invalid"
      return notifyError(error);
    }
    const payload = { 
      email,
      token,
      password
    }
    const result = await updatePassword(payload);

    if(result.status !== 200) {
      return notifyError(result.data.message)
    }

    history.push('/')
    return notifyUser('Successfully Changed Password');

  }

  return (
    <div className="maincontainer">
      <div className="container-fluid">
        <div className="row no-gutter">
          <div className="login d-flex align-items-center py-5">
            <div className="container">
              <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="row">
                <div className="col-lg-10 col-xl-7 mx-auto">
                  <h3 className="display-4"> Forgot Password</h3> 
                  <p className="text-muted mb-4">Enter Your New Password</p>
                    <div className="form-group mb-3">
                      <input id="inputPassword" onChange={(e) => {setPassword(e.target.value)}} type="password" placeholder="Password" required="" className="form-control border-0 shadow-sm px-4 text-primary" />
                    </div>
                    <div className="form-group mb-3">
                      <input id="inputPassword" onChange={(e) => {setConfirmPassword(e.target.value)}} type="password" placeholder="Confirm Password" required="" className="form-control border-0 shadow-sm px-4 text-primary" />
                    </div>
                    <div>
                      <p className="text-muted mb-4">Go back to login <Link to="/login">page</Link></p>
                    </div>
                </div>
                <button onClick={()=> { changePassword() }} className="btn btn-primary col-lg-10 col-xl-7 mx-auto btn-block text-uppercase mb-2 shadow-sm mt-3">Change Pass</button>

              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  )
}

export default ForgotPass
