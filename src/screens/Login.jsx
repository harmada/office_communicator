import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom';
import '../styles/Login.css'
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import validator from 'validator';
import { useDispatch, useSelector } from 'react-redux';
import * as authCreator from '../actions/creator/auth';


const Login = () => {

  const dispatch = useDispatch();
  const history = useHistory();
  const auth = useSelector(state => state.auth);

  const [email, setEmail] = useState('');
  const [password,setPassword] = useState('');

  useEffect(() => {
    auth.auth && history.push('/home');
  },[auth])

  useEffect(() => {
    dispatch(authCreator.loggedInViaToken(localStorage.getItem('token')))
  },[])



  const notifyError = (error) => toast.error(error);


  const login = async () => {
    if(!validator.isEmail(email) || email.length === 0) {
      return notifyError(" Please enter a valid email");
    }
    
    const payload = {
      email,
      password
    }
    dispatch(authCreator.loggedIn(payload));
  }

  return (
      <div className="maincontainer">
        <div className="container-fluid">
          <div className="row no-gutter">
            <div className="col-md-6 d-none d-md-flex bg-image">
            </div>
            <div className="col-md-6 bg-light">
              <div className="login d-flex align-items-center py-5">
                <div className="container">
                  <div className="row">
                    <ToastContainer
                      position="top-right"
                      autoClose={5000}
                      hideProgressBar={false}
                      newestOnTop={false}
                      closeOnClick
                      rtl={false}
                      pauseOnFocusLoss
                      draggable
                      pauseOnHover
                    />
                    <div className="col-lg-10 col-xl-7 mx-auto">
                      <h3 className="display-4"> Office Communicator</h3>
                      <p className="text-muted mb-4">Your Corporate Connection</p>
                        <div className="form-group mb-3">
                          <input id="inputEmail" type="email" onChange={(e)=> { setEmail(e.target.value) }} placeholder="Email address" required="" autofocus="" className="form-control border-0 shadow-sm px-4" />
                        </div>
                        <div className="form-group mb-3">
                          <input id="inputPassword" type="password" onChange={(e)=> { setPassword(e.target.value) }} placeholder="Password" requiredplaceholder="Password" required="" className="form-control border-0 shadow-sm px-4 text-primary" />
                        </div>
                        <div className="form-group justify-content-center-xl">
                          <label className="text-muted mt-3">Forgot Password? Click <Link to="/changepass"> Here </Link></label>
                        </div>
                    </div>
                    <button onClick={()=> { login() }} className="btn btn-primary col-lg-10 col-xl-7 mx-auto btn-block text-uppercase mb-2 shadow-sm mt-3">
                      {
                        auth.loading 
                        ? <>                   
                      
                            <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Loading...
                          </>
                        : <label>SIGN IN</label>
                      }

                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  )
}
export default Login;