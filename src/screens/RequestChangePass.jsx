import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { changePassword } from '../api/auth'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import validator from 'validator';


const RequestChangePassword = () => {

  const [email, setEmail] = useState('');


  useEffect(() => {
  },[])

  const notifyError = (error) => toast.error(error);
  const notifyUser = (data) => toast.success(data);

  const validate = async () => {

    if(!validator.isEmail(email)) {
      return notifyError(" Please enter a valid email");
    }

    const payload = {
      email
    }
    const result = await changePassword(payload);

    if(result.status !== 200) {
      return notifyError(result.data.message)
    }

    return notifyUser('Please check your email for password reset');

  }

  return (
    <div className="maincontainer">
      <div className="container-fluid">
        <div className="row no-gutter">
          <div className="login d-flex align-items-center py-5">
            <div className="container">
              <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="row"> 
              <div className="col-lg-10 col-xl-7 mx-auto">
                  <h3 className="display-4"> Forgot Password</h3> 
                  <p className="text-muted mb-4">Enter Your Email</p>
                    <div className="form-group mb-3">
                      <input id="inputPassword" onChange={(e) => {setEmail(e.target.value)}} type="email" placeholder="Email" required="" className="form-control border-0 shadow-sm px-4 text-primary" />
                    </div>
                    <div>
                      <p className="text-muted mt-4">Go back to login <Link to="/login">page</Link></p>
                    </div>
                </div>
                <button onClick={()=> { validate() }} className="btn btn-primary col-lg-10 col-xl-7 mx-auto btn-block text-uppercase mb-2 shadow-sm mt-3">Request Change Password</button>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RequestChangePassword;
