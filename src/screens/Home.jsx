import React, { useEffect } from 'react'
import '../styles/Home.css'
import SideBar from '../components/SideBar';
import Contacts from '../components/Contacts';
import Message from '../components/Message';
import { useSelector, useDispatch } from 'react-redux'
import * as contactListCreator from '../actions/creator/contacts_list';
import * as contactCreator from '../actions/creator/contact';
import { useHistory } from "react-router-dom";



const Home = () => {

  const contactList = useSelector(state => state.contactList);
  const user = useSelector(state => state.user);
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();


  useEffect(() => {
    dispatch(contactListCreator.contactsList(user.user._id))
  },[])

  useEffect(() => { 
    const { followers } = contactList;
    const payload  = {
      userId: user.user._id,
      follower: followers[0]
    }
    dispatch(contactCreator.saveContact(payload))
  },[contactList])


  useEffect(() => {
    !auth.auth && history.push('/') 
  }, [auth])

  const LazyLoader = () => (
    <div className="lazy-loader">
      <div className="inner-circle">
      </div>
    </div>
  )

  if(contactList.loading) {
    return (
      <div className="lazy-loader-container">
        Loading...
        <LazyLoader />
      </div>
    )
  }

  return (
    <div> 
      <div className="home-body">
        <SideBar/>
        <Message/>
        <Contacts/>
      </div>
    </div>
  )
}

export default Home