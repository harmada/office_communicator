import React from 'react';
import '../styles/Messages.css';
import Header from './Header';
import MessageBox from './MessageBox';
import InputMessageBox from './InputMessageBox';

const Message = () => {

  return (
    <div className="messages">
      <Header/>
      <MessageBox />
      <InputMessageBox/>
    </div>
  )
}

export default Message
