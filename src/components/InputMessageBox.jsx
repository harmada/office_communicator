import React, { useState } from 'react';
import { sendMessage } from '../api/chat';
import { useSelector } from 'react-redux';

const InputMessageBox = () => {

  const contacts = useSelector(state => state.contact)
  const users = useSelector(state => state.user)
  const [message, setMessage] = useState();

  const sendMessageToApi = async (event) => {
    if(event.code === "Enter") {
      
      event.preventDefault()
      const payload = {
        chatRoomId: contacts.contact.chatRoomId,
        userId: users.user._id,
        content: message
      }

      await sendMessage(payload);
      setMessage('')
    }
  }

  return (
    <div>
      <form onSubmit={sendMessage} className="chat-input-container">
        <textarea 
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          type="text" 
          className="input-chat" 
          placeholder="Enter Text Here" 
          onKeyDown={sendMessageToApi}
        />
        <button type="" className="emoji-btn">
          <i className="fas fa-user"></i>
        </button>
      </form>
    </div>
  )
}

export default InputMessageBox
