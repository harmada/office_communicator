import React, { useEffect, useState } from 'react';
import moment from "moment";
import InfiniteScroll from 'react-infinite-scroll-component';
import { io } from "socket.io-client";
import { useSelector, useDispatch } from 'react-redux';
import { server } from '../api/serverUrl';
import * as chatCreator from '../actions/creator/chat';


const MessageBox = () => {

  const socket = io(`${server}`, {
    transports: ["websocket"],
  })
  
  const contact = useSelector(state => state.contact);
  const users = useSelector(state => state.user);
  const message = useSelector(state => state.chat);
  const { chat: { chat, page, pageCount } } = message;
  const [currentPage, setCurrentPage] = useState();
  const [chatRoomId,setChatRoomId] = useState();
  const [chatMessage, setChatMessage] = useState([]);
  const [pageLimit, setPageLimit] = useState();
  const [pageNum, setPageNum] = useState(1);
  const dispatch = useDispatch();

  useEffect(() => {
    setChatRoomId(contact.contact.chatRoomId) 
    return () => {
      socket.off(chatRoomId)
    }
  },[contact])

  useEffect(() => {
    getMessage();
  },[chatRoomId])

  useEffect(() => {
    socket.on(`${chatRoomId}`,() => {
      getMessage();
    })
    return () => {
      socket.off(chatRoomId);
    }
  },[socket])

  useEffect(() => {
    const payload = { 
      chatRoomId: chatRoomId,
      pageNum
    }
    setTimeout(()=>{
      dispatch(chatCreator.fetchMoreMessage(payload))
    },500)  
  },[pageNum])

  useEffect(() => {
    pageCount !== undefined && setPageLimit(pageCount);
    page !== undefined && setCurrentPage(page);
    if(chat !== undefined) {
      setChatMessage(chat);
    } 
  },[chat])


  const getMessage = () => {
    setPageNum(1);
    const payload = { 
      chatRoomId: contact.contact.chatRoomId,
      pageNum: 1
    }
    dispatch(chatCreator.fetchChatMessage(payload))
  }

  const getMoreMessage = () => {
    setTimeout(() => {
      setPageNum(prevPage => prevPage + 1)
    },2000)
  }

  const Chat = ({ message }) => {
    if(message.userId === users.user._id) {
      return (
        <div className="message-container-is-user">
          <img className="message-avatar" src={users.user.image}  alt=""/>
          <div className="message-content-container">
            <div className="message-content">
              { message.content }
            </div>
            <div className="message-time-stamps">
              {moment( message.createdAt).fromNow()}
            </div>
          </div>
        </div>
      )
    }

    return (
      <div className="message-container-other-user">
        <img className="message-avatar" src={contact.contact.image}  alt=""/>
        <div className="message-content-container-other-user">
          <div className="message-content">
            { message.content }
          </div>
          <div className="message-time-stamps">
            {moment( message.createdAt).fromNow()}
          </div>
        </div>
      </div>
    )
  }
  
  return (
    <div className="message-box">
      <div id="scrollableDiv" className="message-box">
        <InfiniteScroll
          style={{ display: 'flex', flexDirection: 'column-reverse' }}
          dataLength={chatMessage.length}
          next={getMoreMessage}
          loadMore={currentPage < pageNum}
          hasMore={pageNum < pageLimit}
          scrollableTarget="scrollableDiv"
          inverse={true}
          endMessage={
            <p style={{ textAlign: 'center' }}>
              <b>No More Message </b>
            </p>
          }
        >
          {
            chatMessage.map((chats, index) => (
              <Chat message={chats} key={index}/>
            ))
          }
        </InfiniteScroll>
      </div>
      {/* { (message.loading || chat === undefined) && <div className="loading-icon"></div> }  */}
    </div>

  )
}

export default MessageBox;
