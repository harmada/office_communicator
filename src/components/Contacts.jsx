import React from 'react';
import { useSelector } from 'react-redux';
import '../styles/Contact.css';
import ContactCards from './ContactCards';
import ContactSearchBar from './ContactSearchBar';

const Contacts = () => {
  const followersData = useSelector(state => state.contactList)


  const ContactsList = () => {
    const { followers } = followersData;
    
    if (followers.length === 0) return null;

    return followers.map((contact, index) => (
      <ContactCards contact={contact} index={index} key={index} />
    ));
  }

  return (
    <div className="contact-list">
      <h3 className="contact-title">
        Contact List
      </h3>
      <ContactSearchBar />
      <div className="contact-grid">
        <ContactsList />
      </div>
    </div>
  )
}

export default Contacts
