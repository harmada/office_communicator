import React from 'react'

const ContactSearchBar = () => {
  return (
    <div className="contact-search-bar">
      <input 
        className="contact-search"
        placeholder="Search"
        type="text"
      />
    </div>
  )
}

export default ContactSearchBar;
