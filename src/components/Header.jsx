import React from 'react'
import '../styles/Messages.css';
import { useSelector } from 'react-redux'


const Header = () => {
  const userData = useSelector(state => state.contact.contact)

  return (
    <div className="header-messages">
      {
        userData.image 
        ? <img className="header-image" src={userData.image} alt=""/>
        : <img className="header-image" src='https://via.placeholder.com/150' alt=""/>
      }
      <div className="header-brand">
        {` ${userData.firstName} ${userData.lastName}`}
      </div>
      <div className="header-options">
        <button className="header-button">
          <i className="fas fa-phone"></i>
        </button>
        <button className="header-button">
          <i className="fas fa-video"></i>
        </button>
      </div>
    </div>
  )
}

export default Header
