import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import * as contactCreator from '../actions/creator/contact';


const ContactCards = (props) => {

  const { contact, index } = props;
  const dispatch = useDispatch();
  const status = useSelector(state => state.contact)
  const user = useSelector(state => state.user)

  const updateMessage = () => { 
    const payload = {
      userId: user.user._id,
      follower: contact
    }
    dispatch(contactCreator.saveContact(payload))
  }
  
  return (
    <button onClick={() => updateMessage()} className={ status.contact._id === contact._id ? 'contact-card-list-active' : 'contact-card-list' } key={index} >
      { 
        contact.image 
        ? <img className="contact-image" src={contact.image} alt=""/>
        : <img className="contact-image" src='https://via.placeholder.com/150' alt=""/>
      }
      <div className="contact-card">
        <div className="contact-name">
          { contact.firstName } { contact.lastName }
        </div>
        <div className='contact-sub'>
          { contact.jobTitle }
        </div>
        <div className='contact-sub'>
          { contact.email }
        </div>
        <div className='contact-sub'>
          { contact.company }
        </div>
      </div>
   
    </button>
  )
}

export default ContactCards
