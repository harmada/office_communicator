import React from 'react'
import '../styles/Sidebar.css';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useHistory } from "react-router-dom";
import * as authCreator from '../actions/creator/auth';



const SideBar = () => {

  const user = useSelector(state => state.user.user)
  const dispatch = useDispatch()
  const history = useHistory();

  const logout = () => {
    dispatch(authCreator.loggedOut());
    history.push('/');
  }

  return (
    <div className="sidebar">
      <div className="sidebar-container"> 
        <div className="sidebar-brand">
          <h1 className="sidebar-name">
            {`${user.firstName} ${user.lastName}`} 
          </h1>
        </div>
        <div className="sidebar-profile">
          <img className="profile-pic" alt="" src={user.image} />
        </div>
        <div className="sidebar-job">
          {user.jobTitle} 
        </div>
        <div className="sidebar-links">
          <div className="sidebar-link">
            <Link to="/home">
              Home
            </Link>
          </div>
          <div className="sidebar-link">
            <Link to="/home">
              Profile
            </Link>
          </div>
          <div className="sidebar-link">
            <Link to="/home">
              Skills
            </Link>
          </div>
        </div>
        <div>
          <button 
            onClick={logout} 
            className="sidebar-logout"
          >
              LogOut
          </button>
        </div>
      </div>
    </div>
  )
}

export default SideBar;
