import { createStore, applyMiddleware } from 'redux';
import allReducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

export const store = createStore( allReducer, composeWithDevTools(applyMiddleware(thunk)));