import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import Login from './screens/Login';
import ForgotPass from './screens/ForgotPass';
import ChangePassword from './screens/RequestChangePass';
import Profile from './screens/Profile';
import Home from './screens/Home';
import Message from "./components/Message";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/login">
          <Login/>
        </Route>
        <Route path="/home">
          <Home/>
        </Route>
        <Route path="/profile">
          <Profile/>
        </Route>
        <Route path="/changepass">
          <ChangePassword/>
        </Route>
        <Route path="/forgotpass">
          <ForgotPass/>
        </Route>
        <Route path="/message">
          <Message/>
        </Route>
        <Route path="/">
          <Redirect to="/login" />
        </Route>
      </Switch>
  </Router>
  );
}

export default App;
